import { Component } from "react";
import { FaLinkedin,FaTwitter,FaPinterest,FaSnapchat,FaInstagram,FaFacebook,FaArrowUp} from 'react-icons/fa'

class FooterComponent extends Component {
 render(){
  return (
   <>
     <div className="container-fluid p-5 orange">
      <div className="row text-center">
        <div className="col-sm-12">
          <h4 className="m-2 font-weight-bold">Footer</h4>
          <a href="#" className="btn btn-dark m-3"><FaArrowUp/>&nbsp; To the top</a>
          <div className="m-2" style={{fontSize:"20px" }}>
            <FaFacebook/>
            <FaInstagram/>
            <FaSnapchat/>
            <FaPinterest/>
            <FaTwitter/>
            <FaLinkedin/>
          </div>
          <h5 className = "font-weight-bold">Powered by DEVCAMP</h5>
        </div>
      </div>
    </div>
   </>
  )
 }
}

export default FooterComponent;