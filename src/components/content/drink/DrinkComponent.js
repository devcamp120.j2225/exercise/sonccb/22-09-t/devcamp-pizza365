import { Component } from "react";


class DrinkComponent extends Component {
 render(){
  return (
   <>
     <div className="col-sm-12 text-center p-4 mt-4">
        <h2 className = "web-text"><b className = "border-bottom border-warning">Chọn đồ uống</b></h2>        
    </div>
      <select id="select-drink-list" className="form-control">
        <option value = "0"> -- Tất cả loại nước uống -- </option>
      </select>
    </>
  )
 }
}

export default DrinkComponent;