import { Component } from "react";


class FormComponent extends Component {
 render(){
  return (
   <>
      <div id="send-order" >
     </div>
       <div className="col-sm-12 text-center p-4 mt-4">
         <h2 className = "web-text"><b className="p-2 mt-4 border-bottom border-warning">Gửi đơn hàng</b></h2>
       </div>              
       {/* Content Input form gửi đơn hàng */}
       <div className="col-sm-12">
             <div className="form-group">
               <label>Tên</label>
               <input className = "form-control" type = "text" id = "inp-name" placeholder = "Nhập tên"/>
             </div>
             <div className="form-group">
               <label>Email</label>
               <input className = "form-control" type = "text" id = "inp-email" placeholder = "Nhập email"/>
             </div>
             <div className="form-group">
               <label >Số điện thoại</label>
               <input className = "form-control" type = "text" id = "inp-phone-number" placeholder = "Nhập số điện thoại"/>
             </div>
             <div className="form-group">
               <label>Địa chỉ</label>
               <input className = "form-control" type = "text" id = "inp-address" placeholder = "Nhập địa chỉ"/>
             </div>
             <div className="form-group">
               <label>Mã giảm giá</label>
               <input className = "form-control" type = "text" id = "inp-voucher-id" placeholder = "Nhập mã giảm giá"/>
             </div>
             <div className="form-group">
               <label >Lời nhắn</label>
               <input className = "form-control" type = "text" id = "inp-message" placeholder = "Nhập lời nhắn"/>
             </div>
             <button type="button" className="btn w-100 web-button font-weight-bold" id = "btn-gui-don">Gửi</button>
      </div>
   </>
  )
 }
}

export default FormComponent;