import { Component } from "react";
import DrinkComponent from "./drink/DrinkComponent";
import FormComponent from "./form/FormComponent";
import IntroduceComponent from "./introduce/IntroduceComponent";
import SizeComponent from "./size/SizeComponent";
import TypeComponent from "./type/TypeComponent";


class ContentComponent extends Component {
 render(){
  return (
   <>
   <div className="container">

    <IntroduceComponent/>
    <SizeComponent/>
    <TypeComponent/>
    <DrinkComponent/>
    <FormComponent/>
   </div>

   </>
  )
 }
}

export default ContentComponent;