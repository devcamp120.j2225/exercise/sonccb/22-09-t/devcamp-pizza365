import { Component } from "react";


class SizeComponent extends Component {
 render(){
  return (
   <>
    <div id="combo" className="row">
                        {/* Title  */}
                        <div className="col-sm-12 text-center p-4 mt-4">
                          <h2 className = "web-text"><b className="p-1 border-bottom border-warning">Chọn size pizza</b></h2>
                          <p><span className="p-2 text-warning">Chọn combo pizza phù hợp với nhu cầu của bạn!</span></p>
                        </div>
                        {/* Plans */}
                        <div className="col-sm-12">
                          <div className="row">
                            <div className="col-sm-4">
                              <div className="card">
                                <div className="card-header text-dark text-center orange">
                                  <h3>S (small)</h3>
                                </div>
                                <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>20cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                    <li className="list-group-item">Salad: <b>200g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                    <li className="list-group-item">
                                      <h1><b>150.000</b></h1> VNĐ
                                    </li>
                                  </ul>
                                </div>
                                <div className="card-footer text-center">
                                  <button className="btn w-100 web-button font-weight-bold" id = "btn-small-click">Chọn</button>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="card">
                                <div className="card-header text-dark text-center orange">
                                  <h3>M (medium)</h3>
                                </div>
                                <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>25cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                    <li className="list-group-item">Salad: <b>300g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                    <li className="list-group-item">
                                      <h1><b>200.000</b></h1> VNĐ
                                    </li>
                                  </ul>
                                </div>
                                <div className="card-footer text-center">
                                  <button className="btn w-100 web-button font-weight-bold" id = "btn-medium-click">Chọn</button>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card">
                                  <div className="card-header text-dark text-center orange">
                                    <h3>L (large)</h3>
                                  </div>
                                  <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                      <li className="list-group-item">Đường kính: <b>30cm</b></li>
                                      <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                      <li className="list-group-item">Salad: <b>500g</b></li>
                                      <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                      <li className="list-group-item">
                                        <h1><b>250.000</b></h1> VNĐ
                                      </li>
                                    </ul>
                                  </div>
                                  <div className="card-footer text-center">
                                    <button className="btn w-100 web-button font-weight-bold" id = "btn-large-click">Chọn</button>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
   </>
  )
 }
}

export default SizeComponent;