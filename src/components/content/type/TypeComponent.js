import { Component } from "react";
import hawaiiPizza from '../../../assets/images/hawaiian.jpg'
import seafoodPizza from '../../../assets/images/seafood.jpg'
import baconPizza from '../../../assets/images/bacon.jpg'


class TypeComponent extends Component {
 render(){
  return (
   <>
    <div id="pizza-type" className="row">
     <div className="col-sm-12 text-center p-4 mt-4">
       <h2 className = "web-text"><b className="p-2 border-bottom border-warning">Chọn loại Pizza</b></h2>
     </div>              
     {/* Content Chọn loại Pizza */}
     <div className="col-sm-12">
       <div className="row">
         <div className="col-sm-4">
           <div className="card w-100" style={{width: "18rem"}}>
             <img src={seafoodPizza} className="card-img-top"/>
             <div className="card-body">
               <h3>OCEAN MANIA</h3>
               <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
               <p>Xốt Cà Chua, Phô Mai Mozzaella, Tôm, Mực, Thanh Cua, Hành Tây.
               </p>
               <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-seafood">Chọn</button></p>
             </div>
           </div>
         </div>
         <div className="col-sm-4">
             <div className="card w-100" style={{width: "18rem"}}>
               <img src={hawaiiPizza} className="card-img-top"/>
               <div className="card-body">
                 <h3>HAWAIIAN</h3>
                 <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                 <p>Xốt Cà Chua, Phô Mai Mozzaella, Thịt Dăm Bông, Thơm.
                 </p>
                 <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-hawaii">Chọn</button></p>
               </div>
             </div>
           </div>
           <div className="col-sm-4">
             <div className="card w-100" style={{width: "18rem"}}>
               <img src={baconPizza} className="card-img-top"/>
               <div className="card-body">
                 <h3>CHEESY CHICKEN BACON</h3>
                 <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                 <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzaella, Cà Chua.
                 </p>
                 <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-bacon">Chọn</button></p>
               </div>
             </div>
           </div>
       </div>
     </div>
   </div>
   </>
  )
 }
}

export default TypeComponent;