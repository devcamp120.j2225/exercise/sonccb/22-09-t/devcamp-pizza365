import { Component } from "react";


class HeaderComponent extends Component {
 render(){
  return (
   <>
    <div className="container-fluid web-navbar-menu">
        <div className="row">
            <div className="col-12">
                <nav className="navbar fixed-top navbar-expand-lg navbar-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav nav-fill w-100">
                            <li className="nav-item active">
                                <a className="nav-link" href="#"> Trang chủ <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#combo"> Combo </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#pizza-type"> Loại pizza </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#send-order"> Gửi đơn hàng </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
   </>
  )
 }
}

export default HeaderComponent;